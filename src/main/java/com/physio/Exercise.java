package com.physio;

/**
 * Created by Trevor on 9/24/16.
 */
public class Exercise {
    private String patientId;
    private String doctorId;
    private String exerciseId;
    private String name;
    private float exertionAngle;
    private float completionRate;
    private float holdTime;

    public Exercise(String patientId, String doctorId, String exerciseId, String name, float exertionAngle, float completionRate, float holdTime) {
        this.patientId = patientId;
        this.doctorId = doctorId;
        this.exerciseId = exerciseId;
        this.name = name;
        this.exertionAngle = exertionAngle;
        this.completionRate = completionRate;
        this.holdTime = holdTime;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getExerciseId() {
        return exerciseId;
    }

    public void setExerciseId(String exerciseId) {
        this.exerciseId = exerciseId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getExertionAngle() {
        return exertionAngle;
    }

    public void setExertionAngle(float exertionAngle) {
        this.exertionAngle = exertionAngle;
    }

    public float getCompletionRate() {
        return completionRate;
    }

    public void setCompletionRate(float completionRate) {
        this.completionRate = completionRate;
    }

    public float getHoldTime() {
        return holdTime;
    }

    public void setHoldTime(float holdTime) {
        this.holdTime = holdTime;
    }
}
