package com.physio;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sqlite.SQLiteDataSource;

import javax.sql.DataSource;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static spark.Spark.*;


/**
 * Class file with main method.
 * Runs the server, builds a model instance and starts the web service.
 */


public class Bootstrap {

    private static final String IP_ADDRESS = "localhost";
    private static final int PORT = 8080;

    private static final Logger logger = LoggerFactory.getLogger(Bootstrap.class);

    public static void main(String[] args) {
        ipAddress(IP_ADDRESS);
        port(PORT);

        //Sub-directory from which to serve static resources (like html and css)
        staticFileLocation("/public");

        DataSource dataSource = configureDataSource();
        if (dataSource == null) {
            System.out.printf("Could not find todo.db in the current directory (%s). Terminating\n",
                    Paths.get(".").toAbsolutePath().normalize());
            System.exit(1);
        }

        //Creates the model instance and then configures and starts the web service
        try {
            PhysioService model = new PhysioService(dataSource);
            new PhysioController(model);
        } catch (PhysioService.PhysioServiceException ex) {
            logger.error("Failed to create a DotsService instance. Aborting");
        }

    }

    /**
     * Checks if the database file exists in the current directory.
     * If it does creates a DataSource instance for the file and returns it.
     * If not, then create a new database file named 'dots.db'
     * @return javax.sql.DataSource corresponding to the dots database
     */
    private static DataSource configureDataSource() {
        Path todoPath = Paths.get(".", "physio.db");
        if ( !(Files.exists(todoPath) )) {
            try { Files.createFile(todoPath); }
            catch (java.io.IOException ex) {
                logger.error("Failed to create physio.db file in current directory. Aborting");
            }
        }
        SQLiteDataSource dataSource = new SQLiteDataSource();
        dataSource.setUrl("jdbc:sqlite:physio.db");
        return dataSource;
    }
}