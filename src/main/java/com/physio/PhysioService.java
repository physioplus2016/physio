package com.physio;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sql2o.Connection;
import org.sql2o.Sql2o;
import org.sql2o.Sql2oException;

import javax.sql.DataSource;
import java.util.*;

/**
 * @author Trevor
 * created on 9/16/16.
 * Main class that handles all the service requests.
 */

public class PhysioService {

    private Sql2o db;

    private final Logger logger = LoggerFactory.getLogger(PhysioService.class);

    private transient static final int MAX_ROW = 5;
    private transient static final int MAX_COL = 4;

    /**
     * Creates Tables in database, if they don't already exist.
     */
    public PhysioService(DataSource dataSource) throws PhysioServiceException {
        db = new Sql2o(dataSource);
        try (Connection conn = db.open()) {
            String sql = "CREATE TABLE IF NOT EXISTS doctor (\n" +
                    "    doctor_id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                    "    name      TEXT, \n" +
                    "    email     VARCHAR DEFAULT \"\",\n" +
                    "    phone     VARCHAR DEFAULT \"\""+
                    ")";
            conn.createQuery(sql).executeUpdate();
            sql = "CREATE TABLE IF NOT EXISTS patient (\n" +
                    "    patient_id INTEGER PRIMARY KEY,\n" +
                    "    name       TEXT,\n" +
                    "    doctor_id  INTEGER REFERENCES doctor (doctor_id) \n" +
                    "                       NOT NULL,\n" +
                    "    notes      VARCHAR DEFAULT \"\", \n" +
                    "    email      VARCHAR DEFAULT \"\",\n" +
                    "    phone      VARCHAR DEFAULT \"\""+
                    ")" ;
            conn.createQuery(sql).executeUpdate();
            sql = "CREATE TABLE IF NOT EXISTS exercise (\n" +
                    "    exercise_id        INTEGER PRIMARY KEY AUTOINCREMENT\n" +
                    "                           NOT NULL,\n" +
                    "    name               TEXT,\n" +
                    "    exertion_angle     NUMERIC DEFAULT (0),\n" +
                    "    completion_rate    NUMERIC DEFAULT (0),\n" +
                    "    hold_time          NUMERIC DEFAULT (0),\n" +
                    "    patient_id         INTEGER REFERENCES patient (patient_id),\n" +
                    "    doctor_id          INTEGER REFERENCES doctor (doctor_id) \n" +
                    ")";
            conn.createQuery(sql).executeUpdate();
            sql = "CREATE TABLE IF NOT EXISTS daily_progress (\n" +
                    "    daily_progress_id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                    "    doctor_id         INTEGER REFERENCES doctor (doctor_id),\n" +
                    "    patient_id        INTEGER REFERENCES patient (patient_id),\n" +
                    "    exercise_id       INTEGER REFERENCES exercise (exercise_id),\n" +
                    "    completion_rate   NUMERIC DEFAULT (0),\n" +
                    "    hold_time         NUMERIC DEFAULT (0),\n" +
                    "    progress_index    NUMERIC DEFAULT (0),\n" +
                    "    pain_index        NUMERIC DEFAULT (0),\n" +
                    "    exertion_angle    NUMERIC DEFAULT (0),\n" +
                    "    date              DATE    DEFAULT (strftime('%Y-%m-%dT%H:%M:%f', 'now', 'localtime') ), \n" +
                    "    patient_comment   VARCHAR,\n" +
                    "    doctor_comment    VARCHAR \n" +
                    ")" ;
            conn.createQuery(sql).executeUpdate();
        } catch(Sql2oException ex) {
            logger.error("Failed to create schema at startup", ex);
            throw new PhysioServiceException("Failed to create schema at startup", ex);
        }
    }

    public void createDoctor(String body) throws PhysioServiceException{
        Doctor doctor = new Gson().fromJson(body, Doctor.class);
        String insertDoctorSql = "INSERT INTO doctor (name, email, phone) " +
                "             VALUES (:name, :email, :phone)" ;

        try (Connection con = db.open()) {
            con.createQuery(insertDoctorSql)
                    .bind(doctor)
                    .executeUpdate();
        }
        catch(Sql2oException ex) {
            logger.error("PhysioService.createPatient: Failed to create a new player", ex);
            throw new PhysioServiceException("PhysioService.createPatient: Failed to create a new player", ex);
        }
    }


    public void createPatient(String doctorId, String body) throws PhysioServiceException{
        //cretae a patient object, insert using that.
        Patient patient = new Gson().fromJson(body, Patient.class);
        patient.setDoctorId(doctorId);
        String insertPatient = "INSERT INTO patient (doctor_id, name, notes, email, phone) " +
                "             VALUES (:doctorId, :name, :notes, :email, :phone)" ;

        try (Connection con = db.open()) {
            con.createQuery(insertPatient)
                    .addColumnMapping("doctorId", "doctorId")
                    .bind(patient)
                    .executeUpdate();
        }
        catch(Sql2oException ex) {
            logger.error("PhysioService.createPatient: Failed to create a new patient", ex);
            throw new PhysioServiceException("PhysioService.createPatient: Failed to create a new patient", ex);
        }
    }


    public List<Patient> getPatients(String doctorId) throws PhysioServiceException {
        String sql = "SELECT * from patient " +
                "WHERE doctor_id = :doctorId";
        try (Connection conn = db.open()) {
            List<Patient> patients = conn.createQuery(sql)
                    .addParameter("doctorId",doctorId)
                    .addColumnMapping("doctor_id", "doctorId")
                    .addColumnMapping("name", "name")
                    .addColumnMapping("patient_id", "patientId")
                    .executeAndFetch(Patient.class);
            return patients;
        } catch(Sql2oException ex) {
            logger.error("PhysioService.getPatients: Failed to query database", ex);
            throw new PhysioServiceException("PhysioService.findAll: Failed to query database", ex);
        }
    }

    public void checkDoctorId(String doctorId) throws PhysioServiceException{
        //Check if id exists in the database
        //If not throw exception

        String sql = "SELECT COUNT(*) FROM doctor WHERE doctor_id = :doctorId ";
        int count;
        try (Connection conn = db.open()) {
            count = conn.createQuery(sql)
                    .addParameter("doctorId", Integer.parseInt(doctorId))
                    .executeScalar(Integer.class);
        } catch(Sql2oException ex) {
            logger.error(String.format("PhysioService.checkDoctorId: Failed to query database for id: %s", doctorId), ex);
            throw new PhysioServiceException(String.format("PhysioService.checkDoctorId: Failed to query database for id: %s", doctorId), ex);
        }
        if(count == 0){
            throw new PhysioServiceException(String.format("PhysioService.checkDoctorId: Failed to query database for id: %s", doctorId), null);
        }

    }

    public void addExercise(String doctorId, String patientId, String body) throws PhysioServiceException{
        Exercise exercise = new Gson().fromJson(body, Exercise.class);
        exercise.setDoctorId(doctorId);
        exercise.setPatientId(patientId);
        String insertPatient = "INSERT INTO exercise (doctor_id, patient_id, name, completion_rate, exertion_angle, hold_time) " +
                "             VALUES (:doctorId, :patientId, :name, :completionRate, :exertionAngle, :holdTime)" ;

        try (Connection con = db.open()) {
            con.createQuery(insertPatient)
                    .bind(exercise)
                    .executeUpdate();
        }
        catch(Sql2oException ex) {
            logger.error("PhysioService.addExercise: Failed to create a new exercise", ex);
            throw new PhysioServiceException("PhysioService.addExercise: Failed to create a new exercise", ex);
        }
    }

    public void updateExercise(String doctorId, String patientId, String exerciseId, String body) throws PhysioServiceException{
        //Update progress_index of a given exercise
        Exercise exercise = new Gson().fromJson(body, Exercise.class);
        exercise.setDoctorId(doctorId);
        exercise.setPatientId(patientId);
        exercise.setExerciseId(exerciseId);
        String sql = "UPDATE exercise SET completion_rate = :completionRate, exertion_angle = :exertionAngle, hold_time = :holdTime " +
                "WHERE doctor_id = :doctorId " +
                "AND patient_id = :patientId " +
                "AND exercise_id = :exerciseId";
        try (Connection conn = db.open()) {
            conn.createQuery(sql)
                    .bind(exercise)
                    .executeUpdate();
        } catch(Sql2oException ex) {
            logger.error(String.format("PhysioService.updateExercise: Failed to update database for exercise"), ex);
            throw new PhysioServiceException("PhysioService.updateExercise: Failed to update database for exercise", ex);
        }


    }

    public List<Exercise> getExercises(String doctorId, String patientId) throws PhysioServiceException{
        String sql = "SELECT * from exercise " +
                "WHERE doctor_id = :doctorId " +
                "AND patient_id = :patientId";
        try (Connection conn = db.open()) {
            List<Exercise> exercise = conn.createQuery(sql)
                    .addParameter("doctorId",doctorId)
                    .addParameter("patientId",patientId)
                    .addColumnMapping("doctor_id", "doctorId")
                    .addColumnMapping("patient_id", "patientId")
                    .addColumnMapping("exercise_id", "exerciseId")
                    .addColumnMapping("completion_rate", "completionRate")
                    .addColumnMapping("hold_time", "holdTime")
                    .addColumnMapping("exertion_angle", "exertionAngle")
                    .executeAndFetch(Exercise.class);
            return exercise;
        } catch(Sql2oException ex) {
            logger.error("PhysioService.getPatients: Failed to query database", ex);
            throw new PhysioServiceException("PhysioService.findAll: Failed to query database", ex);
        }
        //returns a list of exercises objects
    }

    public void addDailyProgress(String doctorId, String patientId, String exerciseId, String body) throws PhysioServiceException{
        DailyProgress dailyProgress = new Gson().fromJson(body, DailyProgress.class);
        dailyProgress.setDoctorId(doctorId);
        dailyProgress.setExerciseId(exerciseId);
        dailyProgress.setPatientId(patientId);
        String insertPatient = "INSERT INTO daily_progress (doctor_id, patient_id, " +
                "exercise_id, hold_time, completion_rate, " +
                "progress_index, exertion_angle, pain_index, patient_comment) " +
                " VALUES (:doctorId, :patientId, :exerciseId, :holdTime, " +
                ":completionRate, :progressIndex, :exertionAngle, :painIndex, :patientComment )" ;
        try (Connection con = db.open()) {
            con.createQuery(insertPatient)
                    .bind(dailyProgress)
                    .executeUpdate();
        }
        catch(Sql2oException ex) {
            logger.error("PhysioService.addExercise: Failed to create a new exercise", ex);
            throw new PhysioServiceException("PhysioService.addExercise: Failed to create a new exercise", ex);
        }
    }

    public List<DailyProgress> getDailyProgress(String doctorId, String patientId, String exerciseId)
            throws PhysioServiceException{
        String sql = "SELECT * from daily_progress " +
                "WHERE doctor_id = :doctorId " +
                "AND patient_id = :patientId " +
                "AND exercise_id = :exerciseId ";
        try (Connection conn = db.open()) {
            List<DailyProgress> dailyProgress = conn.createQuery(sql)
                    .addParameter("doctorId",doctorId)
                    .addParameter("patientId",patientId)
                    .addParameter("exerciseId",exerciseId)
                    .addColumnMapping("doctor_id", "doctorId")
                    .addColumnMapping("patient_id", "patientId")
                    .addColumnMapping("exercise_id", "exerciseId")
                    .addColumnMapping("daily_progress_id", "dailyProgressId")
                    .addColumnMapping("progress_index", "progressIndex")
                    .addColumnMapping("completion_rate", "completionRate")
                    .addColumnMapping("exertion_angle", "exertionAngle")
                    .addColumnMapping("hold_time", "holdTime")
                    .addColumnMapping("pain_index", "painIndex")
                    .addColumnMapping("doctor_comment", "doctorComment")
                    .addColumnMapping("patient_comment", "patientComment")
                    .executeAndFetch(DailyProgress.class);
            return dailyProgress;
        } catch(Sql2oException ex) {
            logger.error("PhysioService.getPatients: Failed to query database", ex);
            throw new PhysioServiceException("PhysioService.findAll: Failed to query database", ex);
        }
    }

    //Returns the patient info
    public PatientInfo getPatientInfo(String patientId) throws PhysioServiceException{

        PatientInfo patientInfo;
        Patient patient;
        Doctor doctor;
        List<DailyProgress> dailyProgress;
        List<Exercise> exercises;

        String sql = "SELECT * from daily_progress " +
                "WHERE patient_id = :patientId ";
        try (Connection conn = db.open()) {
            dailyProgress = conn.createQuery(sql)
                    .addParameter("patientId", patientId)
                    .addColumnMapping("doctor_id", "doctorId")
                    .addColumnMapping("patient_id", "patientId")
                    .addColumnMapping("exercise_id", "exerciseId")
                    .addColumnMapping("daily_progress_id", "dailyProgressId")
                    .addColumnMapping("progress_index", "progressIndex")
                    .addColumnMapping("completion_rate", "completionRate")
                    .addColumnMapping("exertion_angle", "exertionAngle")
                    .addColumnMapping("hold_time", "holdTime")
                    .addColumnMapping("pain_index", "painIndex")
                    .addColumnMapping("patient_comment", "patientComment")
                    .addColumnMapping("doctor_comment", "doctorComment")
                    .executeAndFetch(DailyProgress.class);
        }
        catch(Sql2oException ex) {
            logger.error("PatientInfo.getPatientInfo: Failed to query daily_progress from database", ex);
            throw new PhysioServiceException("PatientInfo.getPatientInfo: Failed to query daily_progress from database", ex);
        }
        sql = "SELECT * from patient " +
                "WHERE patient_id = :patientId ";
        try (Connection conn = db.open()) {
            patient = conn.createQuery(sql)
                    .addParameter("patientId", patientId)
                    .addColumnMapping("doctor_id", "doctorId")
                    .addColumnMapping("name", "name")
                    .addColumnMapping("patient_id", "patientId")
                    .executeAndFetchFirst(Patient.class);;
        } catch(Sql2oException ex) {
            logger.error("PatientInfo.getPatientInfo: Failed to query patient from database", ex);
            throw new PhysioServiceException("PatientInfo.getPatientInfo: Failed to query patient from database", ex);
        }

        sql = "SELECT * from doctor " +
                "WHERE doctor_id = :doctorId ";
        try (Connection conn = db.open()) {
            doctor = conn.createQuery(sql)
                    .addParameter("doctorId", patient.getDoctorId())
                    .addColumnMapping("doctor_id", "doctorId")
                    .addColumnMapping("name", "name")
                    .executeAndFetchFirst(Doctor.class);;
        } catch(Sql2oException ex) {
            logger.error("PatientInfo.getPatientInfo: Failed to query doctor from database", ex);
            throw new PhysioServiceException("PatientInfo.getPatientInfo: Failed to query doctor from database", ex);
        }

        sql = "SELECT * from exercise " +
                "WHERE patient_id = :patientId ";
        try (Connection conn = db.open()) {
            exercises = conn.createQuery(sql)
                    .addParameter("patientId",patientId)
                    .addColumnMapping("doctor_id", "doctorId")
                    .addColumnMapping("patient_id", "patientId")
                    .addColumnMapping("exercise_id", "exerciseId")
                    .addColumnMapping("completion_rate", "completionRate")
                    .addColumnMapping("hold_time", "holdTime")
                    .addColumnMapping("exertion_angle", "exertionAngle")
                    .executeAndFetch(Exercise.class);
        } catch(Sql2oException ex) {
            logger.error("PatientInfo.getPatientInfo: Failed to query exercise from database", ex);
            throw new PhysioServiceException("PatientInfo.getPatientInfo: Failed to query exercise from database", ex);
        }
        patientInfo = new PatientInfo(doctor, patient, exercises, dailyProgress);
        return patientInfo;
    }

    public void addDoctorComment(String doctorId, String patientId, String exerciseId, String dailyProgressId, String body) throws PhysioServiceException{
        //add doctor comments of a given exercise
        DailyProgress dailyProgress = new Gson().fromJson(body, DailyProgress.class);
        dailyProgress.setDoctorId(doctorId);
        dailyProgress.setPatientId(patientId);
        dailyProgress.setExerciseId(exerciseId);
        dailyProgress.setDailyProgressId(dailyProgressId);
        String sql;
        if (dailyProgress.getDoctorComment()!=null) {
            sql = "UPDATE daily_progress SET doctor_comment = :doctorComment " +
                    "WHERE daily_progress_id = :dailyProgressId " +
                    "AND doctor_id = :doctorId " +
                    "AND patient_id = :patientId " +
                    "AND exercise_id = :exerciseId";
        }
        else{
             sql = "UPDATE daily_progress SET patient_comment = :patientComment " +
                    "WHERE daily_progress_id = :dailyProgressId " +
                    "AND doctor_id = :doctorId " +
                    "AND patient_id = :patientId " +
                    "AND exercise_id = :exerciseId";
        }
        try (Connection conn = db.open()) {
            conn.createQuery(sql)
                    .bind(dailyProgress)
                    .executeUpdate();
        } catch(Sql2oException ex) {
            logger.error(String.format("PhysioService.addDoctorComment: Failed to update database for daily progress"), ex);
            throw new PhysioServiceException("PhysioService.addDoctorComment: Failed to update database for daily progress", ex);
        }
    }

    public List<Doctor> getDoctors(String body) throws PhysioServiceException{
        String sql = "SELECT * from doctor";
        try (Connection conn = db.open()) {
            List<Doctor> doctors = conn.createQuery(sql)
                    .addColumnMapping("doctor_id", "doctorId")
                    .executeAndFetch(Doctor.class);
            return doctors;
        } catch(Sql2oException ex) {
            logger.error("PhysioService.getDoctors: Failed to query database", ex);
            throw new PhysioServiceException("PhysioService.getDoctors: Failed to query database", ex);
        }
    }

    static class PhysioServiceException extends Exception {
        PhysioServiceException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}