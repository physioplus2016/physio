package com.physio;

/**
 * Created by Trevor on 9/24/16.
 */
public class Patient {
    private String patientId;
    private String doctorId;
    private String name;
    private String notes;
    private String email;
    private String phone;

    public Patient(String patientId, String doctorId, String name, String notes, String email, String phone) {
        this.patientId = patientId;
        this.doctorId = doctorId;
        this.name = name;
        this.notes = notes;
        this.email = email;
        this.phone = phone;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
