package com.physio;

import java.util.*;


/**
 * Created by Trevor on 9/24/16.
 */

public class PatientInfo {
    private List<DailyProgress> dailyProgressList;
    private Patient patient;
    private Doctor doctor;
    private List<Exercise> exerciseList;

    public PatientInfo( Doctor doctor, Patient patient, List<Exercise> exerciseList, List<DailyProgress> dailyProgressList) {
        this.dailyProgressList = dailyProgressList;
        this.patient = patient;
        this.doctor = doctor;
        this.exerciseList = exerciseList;
    }

    public List<DailyProgress> getDailyProgressList() {
        return dailyProgressList;
    }

    public void setDailyProgressList(List<DailyProgress> dailyProgressList) {
        this.dailyProgressList = dailyProgressList;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public List<Exercise> getExerciseList() {
        return exerciseList;
    }

    public void setExerciseList(List<Exercise> exerciseList) {
        this.exerciseList = exerciseList;
    }
}
