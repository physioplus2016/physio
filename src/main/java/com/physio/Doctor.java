package com.physio;

/**
 * Created by Trevor on 9/24/16.
 */
public class Doctor {
    private String name;
    private String doctorId;
    private String email;
    private String phone;

    public Doctor(String name, String doctorId, String email, String phone) {
        this.name = name;
        this.doctorId = doctorId;
        this.email = email;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
