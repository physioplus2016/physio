package com.physio;

/**
 * Created by Trevor on 9/24/16.
 */
public class DailyProgress {
    private String patientId;
    private String exerciseId;
    private String dailyProgressId;
    private String date;
    private String doctorId;
    private Number holdTime;
    private Number completionRate;
    private Number painIndex;
    private Number exertionAngle;
    private Number progressIndex;
    private String patientComment;
    private String doctorComment;

    public DailyProgress(String patientId, String exerciseId, String dailyProgressId, String date, String doctorId, Number holdTime, Number completionRate, Number progressIndex, Number painIndex, Number exertionAngle, String patientComment, String doctorComment) {
        this.patientId = patientId;
        this.exerciseId = exerciseId;
        this.dailyProgressId = dailyProgressId;
        this.date = date;
        this.doctorId = doctorId;
        this.holdTime = holdTime;
        this.completionRate = completionRate;
        this.progressIndex = progressIndex;
        this.painIndex = painIndex;
        this.exertionAngle = exertionAngle;
        this.patientComment = patientComment;
        this.doctorComment = doctorComment;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getExerciseId() {
        return exerciseId;
    }

    public void setExerciseId(String exerciseId) {
        this.exerciseId = exerciseId;
    }

    public String getDailyProgressId() {
        return dailyProgressId;
    }

    public void setDailyProgressId(String dailyProgressId) {
        this.dailyProgressId = dailyProgressId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public Number getHoldTime() {
        return holdTime;
    }

    public void setHoldTime(Number holdTime) {
        this.holdTime = holdTime;
    }

    public Number getCompletionRate() {
        return completionRate;
    }

    public void setCompletionRate(Number completionRate) {
        this.completionRate = completionRate;
    }

    public Number getProgressIndex() {
        return progressIndex;
    }

    public void setProgressIndex(Number progressIndex) {
        this.progressIndex = progressIndex;
    }

    public Number getPainIndex() {
        return painIndex;
    }

    public void setPainIndex(Number painIndex) {
        this.painIndex = painIndex;
    }

    public Number getExertionAngle() {
        return exertionAngle;
    }

    public void setExertionAngle(Number exertionAngle) {
        this.exertionAngle = exertionAngle;
    }

    public String getPatientComment() {
        return patientComment;
    }

    public void setPatientComment(String patientComments) {
        this.patientComment = patientComments;
    }

    public String getDoctorComment() {
        return doctorComment;
    }

    public void setDoctorComment(String doctorComments) {
        this.doctorComment = doctorComments;
    }
}
