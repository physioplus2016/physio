

package com.physio;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;


import static spark.Spark.*;

/**
 * @author Trevor Bonjour
 * created on 9/23/16.
 *
 * This class is the controller class, that creates the endpoints and calls on PhysioService.
 * All http put, get, post, delete endpoints are specified here.
 */

public class PhysioController {

    private static final String API_CONTEXT = "physio/api";
    private final PhysioService physioService;
    private final Logger logger = LoggerFactory.getLogger(PhysioController.class);


    public PhysioController(PhysioService physioService) {
        this.physioService = physioService;
        setupEndpoints();
    }

    /**
     * Configures all the end points.
     * TODO: Add get patients info from patient id.
     */

    private void setupEndpoints() {

        post(API_CONTEXT + "/doctors", "application/json", (request, response) -> {
            try {
                physioService.createDoctor(request.body());
                response.status(200);
            } catch (PhysioService.PhysioServiceException ex) {
                logger.error(ex.toString());
                response.status(500);
            }
            return Collections.EMPTY_MAP;
        }, new JsonTransformer());

        get(API_CONTEXT + "/doctors", "application/json", (request, response) -> {
            try {
                response.status(200);
                return physioService.getDoctors(request.body());
            } catch (PhysioService.PhysioServiceException ex) {
                logger.error(ex.toString());
                response.status(500);
            }
            return Collections.EMPTY_MAP;
        }, new JsonTransformer());

        //post patient: creates a patient
        post(API_CONTEXT + "/doctors/:doctorId/patients", "application/json", (request, response) -> {
            try {
                String doctorId = request.params(":doctorId");
                physioService.createPatient(doctorId, request.body());
                response.status(200);
            } catch (PhysioService.PhysioServiceException ex) {
                logger.error(ex.toString());
                response.status(500);
            }
            return Collections.EMPTY_MAP;
        }, new JsonTransformer());

        // get patients: checks doctor_id and returns a list of patients
        get(API_CONTEXT + "/doctors/:doctorId/patients", "application/json", (request, response) -> {
            try {
                String doctorId = request.params(":doctorId");
                physioService.checkDoctorId(doctorId);
                response.status(200);
                return physioService.getPatients(doctorId);
            } catch (PhysioService.PhysioServiceException ex) {
                logger.error("Invalid Doctor ID");
                response.status(404);
                return Collections.EMPTY_MAP;
            }
        }, new JsonTransformer());

        // get patient info: Returns patientInfo
        get(API_CONTEXT + "/patients/:patientId", "application/json", (request, response) -> {
            try {
                String patientId = request.params(":patientId");
                response.status(200);
                return physioService.getPatientInfo(patientId);
            } catch (PhysioService.PhysioServiceException ex) {
                logger.error("Invalid Request");
                response.status(404);
                return Collections.EMPTY_MAP;
            }
        }, new JsonTransformer());

        //post exercises: posts exercises for a given patient under a given doctor
        post(API_CONTEXT + "/doctors/:doctorId/patients/:patientId/exercises", "application/json", (request, response) -> {
            try {
                String doctorId = request.params(":doctorId");
                String patientId = request.params(":patientId");
                physioService.checkDoctorId(doctorId);
                //physioService.checkPatientId(patientId);
                physioService.addExercise(doctorId,patientId, request.body());
                response.status(200);
            } catch (PhysioService.PhysioServiceException ex) {
                logger.error(ex.toString());
                response.status(404);

            }
            return Collections.EMPTY_MAP;
        }, new JsonTransformer());



        //put exercise: Updates the progress index of a given exercise
        put(API_CONTEXT + "/doctors/:doctorId/patients/:patientId/exercises/:exerciseId", "application/json", (request, response) -> {
            try {
                String doctorId = request.params(":doctorId");
                String patientId = request.params(":patientId");
                String exerciseId = request.params(":exerciseId");
                physioService.checkDoctorId(doctorId);
                physioService.updateExercise(doctorId, patientId, exerciseId, request.body());
                //physioService.checkPatientId(patientId);
                //physioService.checkExercise(exerciseId);
                response.status(200);
            } catch (PhysioService.PhysioServiceException ex) {
                logger.error("Invalid ID");
                response.status(404);
            }
            return Collections.EMPTY_MAP;
        }, new JsonTransformer());

        put(API_CONTEXT + "/doctors/:doctorId/patients/:patientId/exercises/:exerciseId/daily-progress/:dailyProgressId", "application/json", (request, response) -> {
            try {
                String doctorId = request.params(":doctorId");
                String patientId = request.params(":patientId");
                String exerciseId = request.params(":exerciseId");
                String dailyProgressId = request.params(":dailyProgressId");
                physioService.addDoctorComment(doctorId, patientId, exerciseId, dailyProgressId, request.body());
                response.status(200);
            } catch (PhysioService.PhysioServiceException ex) {
                logger.error("Invalid ID");
                response.status(404);
            }
            return Collections.EMPTY_MAP;
        }, new JsonTransformer());


        // get exercises: returns a list of exercises
        get(API_CONTEXT + "/doctors/:doctorId/patients/:patientId/exercises", (request, response) -> {
            try {
                String doctorId = request.params(":doctorId");
                String patientId = request.params(":patientId");
                response.status(200);
                return physioService.getExercises(doctorId, patientId);
            } catch (PhysioService.PhysioServiceException ex) {
                logger.error("Invalid Doctor ID");
                response.status(404);
                return Collections.EMPTY_MAP;
            }
        }, new JsonTransformer());

        // post daily progress: inserts daily progress for a given exercise
        post(API_CONTEXT + "/doctors/:doctorId/patients/:patientId/exercises/:exerciseId/daily-progress", "application/json", (request, response) -> {
            try {
                String doctorId = request.params(":doctorId");
                String patientId = request.params(":patientId");
                String exerciseId = request.params(":exerciseId");
                physioService.checkDoctorId(doctorId);
                physioService.addDailyProgress(doctorId, patientId, exerciseId, request.body());
                response.status(200);
            } catch (PhysioService.PhysioServiceException ex) {
                logger.error(ex.toString());
                response.status(422);
            }
            return Collections.EMPTY_MAP;
        }, new JsonTransformer());

        // get daily-progress
        get(API_CONTEXT + "/doctors/:doctorId/patients/:patientId/exercises/:exerciseId/daily-progress", "application/json", (request, response) -> {
            try {
                String doctorId = request.params(":doctorId");
                String patientId = request.params(":patientId");
                String exerciseId = request.params(":exerciseId");
                physioService.checkDoctorId(doctorId);
                response.status(200);
                return physioService.getDailyProgress(doctorId, patientId, exerciseId);
            } catch (PhysioService.PhysioServiceException ex) {
                logger.error(ex.toString());
                response.status(422);
                return Collections.EMPTY_MAP;
            }
        }, new JsonTransformer());

        options("/*", (request, response) -> {

            String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }

            String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }

            return "OK";
        });

        before((request, response) -> {
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Request-Method", "*");
            response.header("Access-Control-Allow-Headers", "*");
            // Note: this may or may not be necessary in your particular application
            response.type("application/json");
        });
    }
}
